#reads the abalone dataset and formats into libsvm format
import pandas as pd
import os
train_file='training.data.txt'
test_file='test.data.txt'

os.chdir('C:\\Users\\Natalie Frank\\Documents\\Foundations of Machine Learning\\homework\\HW2\\Code\\Parts 1 and 2')
df=pd.read_csv('abalone.data',header=None)
df.columns=['Sex','Length','Diameter', 'Height','Whole','Shucked','Viscera','Shell','Rings']

df1=df.iloc[:3133,:]
df2=df.iloc[3133:,:]

print("Shape of new dataframes - {} , {}".format(df1.shape, df2.shape)) 
print("Shape of old dataframe - {} ".format(df.shape)) 






def change_format(df, newfile):
    mp={0:'M', 1:'F', 2:'I'}
    f= open(newfile,"w")
    ls=df.shape
    rows=ls[0]
    cols=10
    for r in range(rows):
        lbl= str(int((df.iloc[r,8]<10) & (df.iloc[r,8]>0)))
        f.write(lbl)
        f.write(" ")
        for c in range(cols):
            f.write(str(c+1))
            f.write(":")
            if c<=2:
                f.write(str(int( df.iloc[r,0]==mp[c])))
            else:
                f.write(str(df.iloc[r,c-2]))
            if c<cols-1:
                f.write(" ")
            else:
                 f.write("\n")
    f.flush()
    f.close()



change_format(df1,train_file)
change_format(df2,test_file)

#df=pd.read_csv('text_formating.txt')
#change_format(df, 'neeww.txt')