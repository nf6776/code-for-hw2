#scales the data 
#must be run in same folder as svm-scale, training.data.txt, test.data.txt

$PSDefaultParameterValues['Out-File:Encoding'] = 'default'
.\svm-scale -s scaling_params training.data.txt > training.data.scaled.txt
.\svm-scale -r scaling_params test.data.txt > test.data.scaled.txt