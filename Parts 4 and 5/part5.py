import numpy as np
import os
from part4 import get_folds
from part4 import run_one
from libsvm.svmutil import *


import matplotlib.pyplot as plt



def error_plot(x,y,x_test,y_test,c,flds,degrees):
    cv_errors=[0]*len(degrees)
    test_errors=[0]*len(degrees)
    for i in range(len(degrees)):
        degree=degrees[i]
        vals=run_one(x,y,flds,c,degree)
        cv_errors[i]=100-np.mean(vals)

        model=svm_train(y,x,'-c '+str(c)+' -d '+str(degree)+' -q'+' -t '+str(1))
        p_label, p_acc, p_val = svm_predict(y_test, x_test, model)
        test_errors[i]=(100-p_acc[0])
    

    fig, ax =plt.subplots()
    line1, line2=ax.plot(degrees, cv_errors, 'b', degrees, test_errors , 'g')

    ax.set(xlabel='degree of polynomial kernel', ylabel='percent errors',title='CV error and Test error for c=2^7')

    ax.legend((line1,line2),('CV error', 'Test error'))
    plt.savefig('cv_error_and_test_error.pdf')

def support_vector_plot(x,y,c,flds,degrees):
    folds=len(flds)
    support_vecs=[0]*len(degrees)
    margin_vecs=[0]*len(degrees)
    for i in range(len(degrees)):
        degree=degrees[i]
        for j in range(folds):
            y_curr=[y[j] for j in flds[i]]
            x_curr=[x[j] for j in flds[i]]
            model=svm_train(y_curr,x_curr,'-c '+str(c)+' -d '+str(degree)+' -t '+str(1)+' -q')
            num=model.get_nr_sv()
            support_vecs[i]=support_vecs[i]+ num
            inds=model.get_sv_indices()

            x_supp=[x[ell] for ell in inds]
            y_supp=[y[ell] for ell in inds]
            p_label, p_acc, p_val = svm_predict(y_supp, x_supp, model)

            for ell in range(len(inds)):
                if p_label[ell] == y[ell]:
                    margin_vecs[i]=margin_vecs[i]+1
            

        support_vecs[i]=support_vecs[i]/folds
        margin_vecs[i]=margin_vecs[i]/folds
    fig, ax =plt.subplots()

    line1, line2=ax.plot(degrees, support_vecs, 'b', degrees, margin_vecs , 'g')
    ax.set(xlabel='degree of polynomial kernel', ylabel='average number of vectors',title='Average number of support vectors')
    ax.legend((line1,line2),('number of support vectors', 'number of correctly classified support vectors'))
    plt.savefig('supp_vectors.pdf')




if __name__ == '__main__':
    np.random.seed(100)
    os.chdir('C:\\Users\\Natalie Frank\\Documents\\Foundations of Machine Learning\\homework\\HW2\\Code\\Part 4')

    y,x= svm_read_problem('training.data.scaled.txt')
    y_test,x_test=svm_read_problem('test.data.scaled.txt')
    m=len(y)

    c=2**10
    degree=2

    degrees=range(1,5)
    folds=10
    flds=get_folds(m,folds)
    #error_plot(x,y,x_test,y_test, c,flds,degrees)
    support_vector_plot(x,y,c,flds,degrees)

    
