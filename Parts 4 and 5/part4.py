#from svmutil import *
#import libsvm
import numpy as np
import os
import re
from libsvm.svmutil import *
import sys
import pickle
import matplotlib.pyplot as plt









def get_folds(m, folds):
    perm=np.random.permutation(m)
    d=m//folds
    r= m-d*folds
    flds=[]
    for i in range(folds):
        start=i*d
        end=(i+1)*d
        if i<r:
            end=end+1
        flds.append(perm[start:end])
    return flds

def run_one(x,y,flds, c, degree):
    folds=len(flds)
    vals=[0] *folds
    #stdout_ = sys.stdout #Keep track of the previous value.
    #f=open('myoutputfile.txt', 'w')
    #sys.stdout = f
    for i in range(folds):
        y_curr=[y[j] for j in flds[i]]
        x_curr=[x[j] for j in flds[i]]
        model=svm_train(y_curr,x_curr,'-c '+str(c)+' -d '+str(degree)+' -t '+str(1)+' -q')
        p_label, p_acc, p_val = svm_predict(y, x, model)
        vals[i]=p_acc[0]
    #f.flush()
    #f.close()
    #sys.stdout = stdout_ # restore the previous stdout.

    #f=open('myoutputfile.txt.','r')
    #lines=f.readlines()
    #for i in range(len(lines)):
    #    line=lines[i]
    #    nm=re.sub("[^0-9.]","",line)
    #    vals[i]=float(nm)
    return vals




def cv_data_single(k,degree,flds):
    stdevs=[0]*(2*k+1)
    means=[0]*(2*k+1) 

    for i in range(-k,k+1):
        c=2**i
        print(i)
        cv_errors=run_one(x,y,flds,c, degree)
        stdevs[i+k]=np.std(cv_errors)
        means[i+k]=np.mean(cv_errors)

    with open('stds_and_means_'+str(degree)+'.txt', 'wb') as f:
        pickle.dump([means,stdevs],f)

    
def plot_single(degree):
    with open('stds_and_means_'+str(degree)+'.txt', 'rb') as f:
        means, stdevs=pickle.load(f)
    
    k=int((len(means)-1)/2)

    x= range(-1*k,k+1)
    errors=[100-means[i] for i in range(len(means))]

    mpstd=[errors[i]+stdevs[i] for i in range(len(means))]# means plus one standard deviation
    mmstd=[errors[i]-stdevs[i] for i in range(len(means))]# means minus one standard deviation
    fig, ax =plt.subplots()

    ax.plot(x, errors)
    ax.plot(x, mpstd, 'r--', x, mmstd , 'r--')

    
    ax.set(xlabel='log(c)', ylabel='percent errors',title='Polynomial Kernel of degree '+str(degree))
    plt.savefig('cv_error_plot_degree_'+str(degree)+".pdf")
    
if __name__ == '__main__':
    np.random.seed(100)
    os.chdir('C:\\Users\\Natalie Frank\\Documents\\Foundations of Machine Learning\\homework\\HW2\\Code\\Parts456')
    y,x= svm_read_problem('training.data.scaled.txt')
    m=len(y)    


    folds=10
    flds=get_folds(m,folds)


    #for degree in range(1,5):
    #    k=20
    #    cv_data_single(k,degree,flds)

    


    #for degree in range(1,5):
    #    with open('stds_and_means_'+str(degree)+'.txt', 'rb') as f:
    #       means, stdevs=pickle.load(f)
    #    print(means)
    #    plot_single(degree)

    with open('stds_and_means_'+str(2)+'.txt', 'rb') as f:
        means, stdevs=pickle.load(f)
        i=10
        ind=20+1+i-1
        best=means[ind]
        print(best)
        print(100-best)
        print(means)
    